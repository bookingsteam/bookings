var google = require('googleapis');
var OAuth2 = google.auth.OAuth2;
var TOKEN_DIR = './';
var TOKEN_PATH = './token.json'
var fs = require('fs');

// generate a url that asks permissions for Google+ and Google Calendar scopes
var scopes = [
    'https://www.googleapis.com/auth/plus.me',
    'https://www.googleapis.com/auth/calendar'
];

const DEFAULT_APP_URL = 'http://localhost:4200/';
function authorize(req, res, next) {

    var oauth2Client = new OAuth2(
        '345671078583-1emdbpf8d06uhf0u6ga3jmeljgb9utfr.apps.googleusercontent.com',
        'uY-VIJ4FTGfF9a-gOy-uTHkV',
        'http://localhost:1914/token'
    );

    req.oauth2Client = oauth2Client;

    fs.readFile(TOKEN_PATH, function (err, token) {
        if (err) {
            console.log(err, 'readfile error');
            getNewToken(req, res);
        } else {

            req.oauth2Client.credentials = JSON.parse(token);
            return next();
        }
    });
}

function getNewToken(req, res) {
    var authUrl = req.oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: scopes
    });
    res.status(401)
        .json({ success: false, msg: "Not google authorized", authUrl: authUrl, code: "NOT_AUTHORIZED" });
}

function getTokens(req, res, next) {
    var oauth2Client = new OAuth2(
        '345671078583-1emdbpf8d06uhf0u6ga3jmeljgb9utfr.apps.googleusercontent.com',
        'uY-VIJ4FTGfF9a-gOy-uTHkV',
        'http://localhost:1914/token'
    );

    req.oauth2Client = oauth2Client;

    if (!req.query.code) {
        res.json({ error: 'no query string' });
    } else {
        req.oauth2Client.getToken(req.query.code, function (err, tokens) {
            if (!err) {
                storeToken(tokens, function () {
                    res.redirect(DEFAULT_APP_URL);
                });

            } else {
                res.json({ error: 'Problem with token' });
            }
        });
    }

}

function refreshToken(req, res) {

    req.oauth2Client.refreshAccessToken(function (err, tokens) {
        if (!err) {
            storeToken(tokens, function () {
                console.log("Redirecting...");
                return next();
                // res.redirect(req.originalUrl);
            });
            return;
        }
        fs.unlinkSync(TOKEN_PATH);
        res.status(401).json({ success: false, msg: "Not google authorized", authUrl: req.originalUrl, code: "NOT_AUTHORIZED" });

    });
}

function storeToken(token, callback) {
    try {
        fs.mkdirSync(TOKEN_DIR);
    } catch (err) {
        if (err.code != 'EEXIST') {
            throw err;
        }
    }
    fs.writeFile(TOKEN_PATH, JSON.stringify(token), callback);
    console.log('Token stored to ' + TOKEN_PATH);
}

exports.authorize = authorize;
exports.refreshToken = refreshToken;
exports.getTokens = getTokens;