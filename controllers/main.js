'use strict'
/**
 * GET /
 * Home page.
 */
var google = require('googleapis');
var mysql = require('../lib/mysqlConnection/MysqlConnection'),
    config = require('../config'),
    moment = require('moment'),
    async = require('async'),
    _ = require('lodash'),
    Utils = require('../lib/utils'),
    polyline = require('polyline'),
    googleAuth = require('../utils/googleAuthManager');
var UserRepository = require('../api/user/repositories/UserRepository')(mysql, config);

// var PagesRepository = require('../api/pages/repositories/ArticlesRepository')(mysql, config);
// let EventsRepository = require('../api/events/repositories/ArticlesRepository')(mysql, config);
// let CareersRepository = require('../api/careers/repositories/ArticlesRepository')(mysql, config);
// let StoreRepository = require('../api/store/repositories/ArticlesRepository')(mysql, config);

// var i18n = require('i18n-express');
module.exports = function MainController() {
    let index = (req, res) => {
        console.log(12345);
        res.render('index', {
            title: 'Chill'
        });
    };

    let schedule = (req, res) => {
        var calendar = google.calendar('v3');
        calendar.events.list({
            auth: req.oauth2Client,
            calendarId: 'primary',
            timeMin: (new Date()).toISOString(),
            maxResults: 10,
            singleEvents: true,
            orderBy: 'startTime'
        }, function (err, response) {
            if (err) {
                console.log('The API returned an error: ' + err);
                googleAuth.refreshToken(req, res);
                return;
            }
            var events = response.items;
            if (events.length == 0) {
                console.log('No upcoming events found.');
                res.json("No events!");
            } else {
                console.log('Upcoming 10 events:');
                for (var i = 0; i < events.length; i++) {
                    var event = events[i];
                    var start = event.start.dateTime || event.start.date;
                    console.log('%s - %s', start, event.summary);
                }
                res.json({ events: events })
            }
        });
    }

    let forTests = (req, res) => {
        let tplData = {
            names: 'Anton Harizanov',
            date: '19/04/2017 | 12:37',
            heading: 'Heading',
            body: 'Text text twxt',
            link: '',
            linkText: 'Learn more'
        };
        res.render('partials/email', tplData);
    };

    let screen = (req, res) => {
        let tplData = {
            w: req.query.w || "100%",
            h: req.query.h || "1000",
        };
        res.render('test', tplData);
    };

    let pages = (req, res) => {
        let routeName = req.url.replace(/\W+/, "");
        let data = pagesData[routeName];
        res.render('public/main-page', {
            title: 'Home',
            page: data
        });
    };

    let displayResetPassword = (req, res) => {
        res.render('reset-password', {
            title: 'Forgotten password'
        });
    };

    let displayChangePassword = (req, res) => {
        let token = req.query.token || "";
        res.render('change-password', {
            title: 'Change password',
            token: token
        });
    };

    return {
        index: index,
        displayResetPassword: displayResetPassword,
        displayChangePassword: displayChangePassword,
        forTests: forTests,
        screen: screen,
        schedule: schedule
        // pages: pages
    };
}