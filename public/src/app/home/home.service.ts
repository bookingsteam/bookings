import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class HomeService {

    apiUrl: string = 'http://localhost:1914/apiv1/';

    constructor(private http: HttpClient) { }


    loadCalendar(): Observable<any> {
        return this.http.get(this.apiUrl + 'calendar/calendar'); // TODO: redundant calendar
    }

    loadWorkingHours(): Observable<any> {
        return this.http.get('load-working-hours');
    }

    saveWorkingHours(data: any): Observable<any> {
        return this.http.put('update-schedule', data);
    }

    authorize(data: any): Observable<any> {
        return this.http.post('authorize', data);
    }
}
