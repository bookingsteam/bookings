exports.asyncFromPromise = function(promise, name) {
    return function (callback) {
        promise
            .then(function (results) {
                return callback(null, results);
            })
            .fail(function (err) {
                return callback(err, null);
            });
    };
};

exports.timeFilter = function (input) {
    var sec_num = parseInt(input, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
};
exports.getStaticGmapURLForDirection = function(locations, encoded){
    let size = '1200x900',
        zoom = 15,
        scale = 1,
        API_KEY = "AIzaSyAqqBFH8g0Jfm3zfEbUMq86vusmqHnYQwU",
        color = "0x00aa4f99",
        staticMapLocation = [];

    locations.forEach(location => {
        staticMapLocation.push([location.lat, location.lng].join());
    });

    let path = staticMapLocation.join('|'),
        startCoords = staticMapLocation[0],
        finishCoords = staticMapLocation[staticMapLocation.length - 1];
    if (encoded) {
        path = "enc:" + encoded
    }
    return `http://maps.googleapis.com/maps/api/staticmap?size=${size}&scale=${scale}&zoom=${zoom}&key=${API_KEY}&path=color:${color}|weight:5|${path}&markers=color:green|label:A|size:tiny|${startCoords}&markers=color:red|label:A|size:tiny|${finishCoords}`;
}