var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const compression = require('compression');
var cookieParser = require('cookie-parser');
const sass = require('node-sass-middleware');
var config    = require('./config');
// var app = express();
var app = module.exports = express();


global.catchError = function catchError(res, err, source) {
  if (process.env.NODE_ENV !== 'production') {
      console.log(source, err);
      res.status(400).json({success: false, msg: err})
  } else {
      res.status(400).json({success: false})
  }
}

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(compression());
app.use(sass({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public')
}));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressValidator());
app.use(cookieParser());
// setup the logger
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
/*app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://192.168.1.172:8080");
  res.header("Access-Control-Allow-Credentials", "true");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE")
  next();
});*/
require('./routes')(app);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.set('jwtTokenSecret', 'SAMO_LITEX');
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
  console.error('Error:::', err.message);
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  console.error('Error:::', err.message);
  res.render('error', {
    message: err.message,
    error: {}
  });
});
app.listen(config.port, function () {
  console.log('Example app listening on port '+config.port+'!');
});

module.exports = app;