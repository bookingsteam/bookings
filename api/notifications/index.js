var mysql = require('../../lib/mysqlConnection/MysqlConnection'),
	config = require('../../config');
	
module.exports = function (app) {
	
	var NotificationsRepository = new(require('./repositories/NotificationsRepository'))(mysql, config),
		NotificationsController = new (require('./controllers/NotificationsController'))(NotificationsRepository);
		
	return require ('./routes/NotificationsRoutes')(NotificationsController);
};