var Q = require('q');
var Utils   = require('../../../lib/utils');
var async   = require('async');
var fs = require('fs');
var crypto = require('crypto');
var S3FS = require('s3fs');
var s3fsImpl = new S3FS('vetsapp2', {
    accessKeyId: 'AKIAJRZVAM25KKQURZWA',
    secretAccessKey: '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'
});
var ntfSender = new (require('../../NotificationSender'));

module.exports = function NotificationsController(NotificationsRepository) {

//NTF TYPES:
var urlNotification = 1
var dogShareNotification = 2

    function createNotificationFromData(data, type, callback) {
        NotificationsRepository.createNotification(data).then(function(insertId){
                    if (type == 2) {
                        setTimeout(function(){
                         ntfSender.sendShareNotification(data);
                     }, 3000);
                        
                    } else {
                        if (data.targetUser == 0) {
                            NotificationsRepository.getAllUsersRegisteredForNotifications().then(function(users){
                                for (var i = users.length - 1; i >= 0; i--) {
                                    data["deviceId"] = users[i].device_id
                                    ntfSender.sendUrlNotification(data)
                                }
                            })
                            
                        } else {
                            NotificationsRepository.getDeviceIDForUser(data["targetUser"]).then(function(users){
                               
                                    data["deviceId"] = users[0].device_id
                                    ntfSender.sendUrlNotification(data)
                                
                            })
                        }
                        
                    }
                    callback(true);
                    }).catch(function(err){
                        console.log(err);
                        callback(false);
                    });
    }

    function createNotification (req, res, next) {
        var data = {
            fromUser: req.body.fromUser,
            targetUser: req.body.targetUser,
            type: req.body.type,
            url: req.body.url,
            photoUrl: req.body.url,
            content: req.body.content
        }
        createNotificationFromData(data, data.type, function(success){
            res.json({success:success});
        });
    }

    function seeNotifications(req, res, next) {
        NotificationsRepository.seeNotifications(req.user.id).then(function(success){
            
                res.json({success:success});
            
        }).catch(function(err){
                        console.log(err);
                    });
    }

    function visitNotification(req, res, next) {
        NotificationsRepository.visitNotification(req.params.id).then(function(success){
            
                res.json({success:success});
            
        }).catch(function(err){
                        console.log(err);
                    });
    }

    function removeDeviceIDForUser(req, res, next){
        NotificationsRepository.removeDeviceIDForUser(req.user.id).then(function(success){
            
                res.json({success:success});
            
        }).catch(function(err){
                        console.log(err);
                    });
    }

    return {
        createNotification: createNotification,
        createNotificationFromData: createNotificationFromData,
        seeNotifications: seeNotifications,
        visitNotification: visitNotification,
        removeDeviceIDForUser: removeDeviceIDForUser
    };
};