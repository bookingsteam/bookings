var Q = require('q');
var moment = require('moment');

module.exports = function NotificationsRepository(mysql, config) {

    function createNotification(ntfData){
 
      var query = [
            'INSERT INTO `vets`.`notifications` (`from_user`, `target_user`, `url`, `photo_url`, `content`, `type`, `timestamp`)',
            'VALUES (:fromUser, :targetUser, :url, :photoUrl, :content, :type, NOW()); '
        ].join(" ");

        return mysql.makeQuery(query, {
            fromUser: parseInt(ntfData.fromUser),
            targetUser: parseInt(ntfData.targetUser),
            url: ntfData.url,
            photoUrl: ntfData.photoUrl,
            content: ntfData.content,
            type: parseInt(ntfData.type)
        }, function (result) {
            return result;
        });
    }

    function seeNotifications(userId){
        var query = [
            'UPDATE notifications',
            'SET seen = 1',
            'WHERE target_user = :userId AND seen = 0'
        ].join(" ");


        return mysql.makeQuery(query, {
            userId: userId
        }, function (result) {
            return result;
        });
    }

    function visitNotification(notificationId){
        var query = [
            'UPDATE notifications',
            'SET visited = 1',
            'WHERE id = :notificationId'
        ].join(" ");


        return mysql.makeQuery(query, {
            notificationId: notificationId
        }, function (result) {
            return result;
        });
    }

    function getAllUsersRegisteredForNotifications(){
        var query = [
            'SELECT device_id FROM users',
            'WHERE device_id != NULL;'
        ].join(" ");
        return mysql.makeQuery(query, null, function (result) {
            return result;
        });
    }

    function getDeviceIDForUser(userId){
        var query = [
            'SELECT device_id FROM users',
            'WHERE id = :userId;'
        ].join(" ");


        return mysql.makeQuery(query, {
            userId: userId
        }, function (result) {
            return result;
        });
    } 

    function removeDeviceIDForUser(userId){
        var query = [
            'UPDATE users',
            'SET device_id = NULL',
            'WHERE id = :userId;'
        ].join(" ");


        return mysql.makeQuery(query, {
            userId: userId
        }, function (result) {
            return result;
        });
    }  

    return {
        createNotification: createNotification,
        seeNotifications: seeNotifications,
        visitNotification: visitNotification,
        getAllUsersRegisteredForNotifications: getAllUsersRegisteredForNotifications,
        getDeviceIDForUser: getDeviceIDForUser,
        removeDeviceIDForUser: removeDeviceIDForUser
    };
}