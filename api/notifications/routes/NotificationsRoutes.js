var express = require('express');
var router = express.Router();
var jwtauth = require('../../user/jwtAuth.js');
var multiparty = require('connect-multiparty'),
    multipartyMiddleware = multiparty();

module.exports = function(NotificationsController){

	router.get('/see', jwtauth, NotificationsController.seeNotifications);
	router.get('/visit/:id', jwtauth, NotificationsController.visitNotification);
	router.post('/create', jwtauth, NotificationsController.createNotification);
	router.get('/removeDeviceToken', jwtauth, NotificationsController.removeDeviceIDForUser);

    return router;
};