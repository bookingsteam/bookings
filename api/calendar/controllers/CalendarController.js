const google = require('googleapis');
const OAuth2 = google.auth.OAuth2;

const oauth2Client = new OAuth2(
    '345671078583-1emdbpf8d06uhf0u6ga3jmeljgb9utfr.apps.googleusercontent.com',
    'uY-VIJ4FTGfF9a-gOy-uTHkV',
    'http://localhost:1914/apiv1/calendar/tokens'
);

// generate a url that asks permissions for Google+ and Google Calendar scopes
const scopes = [
    'https://www.googleapis.com/auth/plus.me',
    'https://www.googleapis.com/auth/calendar'
];

const TOKEN_DIR = './';
const STORAGE_DIR = './storage/';

const TOKEN_PATH = './token.json';
const CALENDAR_PATH = STORAGE_DIR + 'calendar.json';
const DEFAULT_CALENDAR_NAME = 'SmartBookings';
const fs = require('fs');

module.exports = function CalendarController() {

    function getCalendar(req, res, next) {
        /* Callback Hell --> use await */
        fs.readFile(CALENDAR_PATH, function (err, calendar) {
            if (err) {
                authorize(function () {
                    createCalendar(function (err, response) {
                        if (err) {
                            console.error(err);
                            res.status(500).json({
                                error: err
                            });
                        } else {
                            _storeCalendarData(response, function () {
                                res.json({
                                    success: true
                                })
                            });
                        }
                    });
                }, res);
                // authorize(listEvents, res)
            } else {

                fs.readFile(CALENDAR_PATH, function (err, calendarData) {
                    if (err) {
                        console.log(err, 'Calendar -> readfile error');
                        res.status(500).json({
                            error: err
                        });
                    } else {
                        res.json({
                            calendar: JSON.parse(calendarData)
                        });
                    }
                });
            }
        });
    }
    function _storeCalendarData(data, callback) {
        try {
            fs.mkdirSync(STORAGE_DIR);
        } catch (err) {
            if (err.code != 'EEXIST') {
                throw err;
            }
        }
        fs.writeFile(CALENDAR_PATH, JSON.stringify(data), callback);
    }
    function authorize(callback, res) {
        fs.readFile(TOKEN_PATH, function (err, token) {
            if (err) {
                console.log(err, 'readfile error');
                getNewToken(res);
            } else {
                oauth2Client.credentials = JSON.parse(token);
                callback(res);
            }
        });
    }

    function getNewToken(res) {
        var authUrl = oauth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: scopes
        });
        res.redirect(authUrl);
    }

    function getAuthURL(req, res, next) {
        var url = oauth2Client.generateAuthUrl({
            // 'online' (default) or 'offline' (gets refresh_token)
            access_type: 'offline',

            // If you only need one scope you can pass it as a string
            scope: scopes

            // Optional property that passes state parameters to redirect URI
            // state: 'foo'
        });

        //res.json({url: url});

    }

    function getTokens(req, res, next) {
        if (!req.query.code) {
            res.json({ error: 'no query string' })
        }
        oauth2Client.getToken(req.query.code, function (err, tokens) {
            if (err) {
                console.log(err, "GET TOKENS");
                // getNewToken(listEvents, res);
            } else {
                oauth2Client.credentials = tokens;
                storeToken(tokens, function () {
                    res.redirect('/apiv1/calendar/calendar');
                });

            }
        });
    }

    /*function listEvents(res) {
        var calendar = google.calendar('v3');
        calendar.events.list({
            auth: oauth2Client,
            calendarId: 'primary',
            timeMin: (new Date()).toISOString(),
            maxResults: 10,
            singleEvents: true,
            orderBy: 'startTime'
        }, function (err, response) {
            if (err) {
                console.log('The API returned an error: ' + err);
                refreshToken(res, function () {
                    listEvents(res);
                });
                return;
            }
            var events = response.items;
            if (events.length == 0) {
                console.log('No upcoming events found.');
                res.json("No events!");
            } else {
                console.log('Upcoming 10 events:');
                for (var i = 0; i < events.length; i++) {
                    var event = events[i];
                    var start = event.start.dateTime || event.start.date;
                    console.log('%s - %s', start, event.summary);
                }
                res.json({ events: events })
            }
        });
    }*/

    function refreshToken(res, completion) {

        oauth2Client.refreshAccessToken(function (err, tokens) {
            if (!err) {
                storeToken(tokens, completion);
                return;
            }
            fs.unlinkSync(TOKEN_PATH);
            authorize(completion, res);
            //res.json({error: err});

        });
    }

    function storeToken(token, callback) {
        try {
            fs.mkdirSync(TOKEN_DIR);
        } catch (err) {
            if (err.code != 'EEXIST') {
                throw err;
            }
        }
        fs.writeFile(TOKEN_PATH, JSON.stringify(token), callback);
        console.log('Token stored to ' + TOKEN_PATH);
    }

    function addWeeklySchedule(req, res, next) {
        for (var i = 0; i < 7; i++) {
            var event = {
                'summary': 'Google I/O 2015',
                'location': '800 Howard St., San Francisco, CA 94103',
                'description': 'A chance to hear more about Google\'s developer products.',
                'start': {
                    'dateTime': '2018-01-' + (15 + [i]).toString() + 'T' + '17:00:00-07:00',
                    'timeZone': 'Sofia, Bulgaria',
                },
                'end': {
                    'dateTime': '2018-01-' + (15 + [i]).toString() + 'T' + '17:00:00-07:00',
                    'timeZone': 'Sofia, Bulgaria',
                },
                'recurrence': [
                    'RRULE:FREQ=DAILY;COUNT=2'
                ],
                'attendees': [
                    { 'email': 'lpage@example.com' },
                    { 'email': 'sbrin@example.com' },
                ],
                'reminders': {
                    'useDefault': false,
                    'overrides': [
                        { 'method': 'email', 'minutes': 24 * 60 },
                        { 'method': 'popup', 'minutes': 10 },
                    ],
                },
            };

            calendar.events.insert({
                auth: auth,
                calendarId: 'primary',
                resource: event,
            }, function (err, event) {
                if (err) {
                    console.log('There was an error contacting the Calendar service: ' + err);
                    return;
                }
                console.log('Event created: %s', event.htmlLink);
            });
        }

    }

    function openScheduler(req, res, next) {
        authorize(function (res) {

        }, res);
    }

    function addSchedule(req, res, next) {
        var calendarId = 'uoqhu1rgi5qi2u5gh10vmiodf4@group.calendar.google.com'; //TODO-Extract from storage
        var calendar = google.calendar('v3');
        var event = {
            'summary': 'Мартин на фризьор от 10',
            'location': 'София, бул. България 49B',
            'description': 'A chance to hear more about Google\'s developer products.',
            'start': {
                'dateTime': new Date('03/01/2018 10:00'),
                'timeZone': 'Sofia, Bulgaria',
            },
            'end': {
                'dateTime': new Date('03/01/2018 10:30'),
                'timeZone': 'Sofia, Bulgaria',
            },
            'reminders': {
                'useDefault': false,
                'overrides': [
                    { 'method': 'email', 'minutes': 24 * 60 },
                    { 'method': 'popup', 'minutes': 10 },
                ],
            },
        };

        calendar.events.insert({
            auth: oauth2Client,
            calendarId: calendarId,
            resource: event,
        }, function (err, event) {
            if (err) {
                res.json({ error: err });
            }
            res.json({ event: event });
        });

    }

    function createCalendar(callback) {
        var calendar = google.calendar('v3');
        calendar.calendars.insert({
            auth: oauth2Client,
            resource: {
                summary: DEFAULT_CALENDAR_NAME
            }
        }, callback);

    }



    return {
        addWeeklySchedule: addWeeklySchedule, // Ne e public
        getCalendar: getCalendar, // Ne e public
        getTokens: getTokens, // PUBLIC
        addSchedule: addSchedule, // Ne e public
        createCalendar: createCalendar // Ne e public
    }
}