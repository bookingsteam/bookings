var express = require('express');
var router = express.Router(),
	googleAuth = require('../../../utils/googleAuthManager');

module.exports = function(CalendarController){
    router.get('/calendar', googleAuth.authorize, CalendarController.getCalendar);
    router.get('/tokens', CalendarController.getTokens);
    router.post('/add-schedule', googleAuth.authorize, CalendarController.addSchedule);
    // router.post('/create-calendar', googleAuth.authorize, CalendarController.createCalendar);

    return router;
};
