var mysql = require('../../lib/mysqlConnection/MysqlConnection'),
	config = require('../../config');
	
module.exports = function () {
	
	// var UserRepository = new(require('./repositories/UserRepository'))(mysql, config),
	var CalendarController = new (require('./controllers/CalendarController'))();
		
	return require ('./routes/CalendarRoutes')(CalendarController);
};