var fs = require('fs');
var Q = require('q');
var nodemailer = require('nodemailer');
var moment = require('moment');
var async   = require('async');
var Utils   = require('../../../lib/utils');
var config = require('../../../config');
var crypto = require('crypto');
var jwt = require('jwt-simple');
var S3FS = require('s3fs');
var s3fsImpl = new S3FS('vetsapp4', {
    accessKeyId: 'AKIAI5BHFB6WMJVWKVXA',
    secretAccessKey: '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',
    signatureVersion: "v4"
});

var ntfManager = new (require('../../NotificationsManager'));
const emailTemplate = './../../views/partials/email.ejs';

module.exports = function UserController(UserRepository, app) {
    function login(req, res, next){
        UserRepository.loginUser(req.body.email, req.body.password).then(function(user){
            if (user) {
                var expires = moment().add(7, 'days').valueOf();
                var secret = app.get('jwtTokenSecret');
                var token = jwt.encode(
                    {
                        iss: user.userId,
                        exp: expires
                    },
                    app.get('jwtTokenSecret')
                );
        console.log(req.body);
                if (req.body.deviceToken != null) {
                    if (req.body.deviceToken.length > 0) {
                        UserRepository.setDeviceToken(req.body.deviceToken, user.userId);
                    }
                }


                res.json({
                    success: true,
                    user: user,
                    token: token,
                    expires: expires
                });


            }
            else{
                res.json({
                    success: false,
                    msg: "Wrong user or password"
                });
            }
        });
    }
    async function register(req, res, next){
        let error = null;
        let isValidEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(req.body.email);
        if (!req.body.email || !isValidEmail) {
            error = "Моля, въведете валиден E-mail адрес";
        } else if(!req.body.password){
            error = "Моля, въведете парола";
        }
        if (error) {
            res.json({
                success: false,
                msg: error
            });
            return;
        }

        try {
            let isExisting = await UserRepository.checkIfUserExists(req.body.email);
            if (isExisting === true) {
                res.json({
                    success: false,
                    msg: "User already exists"
                });
            } else {
                let user = await UserRepository.registerUser({
                    email: req.body.email,
                    password: req.body.password,
                    name: req.body.name,
                    surname: req.body.surname
                });
                res.json({
                    success: true,
                    user: user
                })
            }
        } catch(err) {
            catchError(res, err);
        }

    }
    function socialNetworkSignUp(req, res, next){
        UserRepository.checkIfUserExists(req.body.email).then(function(isExisting){
            if (isExisting === true) {
                UserRepository.getUserIdByEmail(req.body.email).then(function(userId){
                    UserRepository.forceLogin(userId).then(function(user){
                        if (user) {
                            if (req.body.deviceToken != null) {
                                if (req.body.deviceToken.length > 0) {
                                    UserRepository.setDeviceToken(req.body.deviceToken, user.userId);
                                }
                            }
                            _setLoggedUserDataAndToken(user, res);
                        }
                        else{
                            res.json({
                                success: false,
                                msg: "No such user!"
                            });
                        }
                    });
                });
            }
            else{
                UserRepository.registerUserBySocialNetwork({
                    email: req.body.email,
                    name: req.body.name,
                    photo: req.body.photo
                }).then(function(userId){
                    UserRepository.forceLogin(userId).then(function(user){
                        if (user) {
                            if (req.body.deviceToken != null) {
                                if (req.body.deviceToken.length > 0) {
                                    UserRepository.setDeviceToken(req.body.deviceToken, user.userId);
                                }
                            }
                            _setLoggedUserDataAndToken(user, res);
                        }
                        else{
                            res.json({
                                success: false,
                                msg: "No such user!"
                            });
                        }
                    });
                }).catch(function(err){
                    console.log(err);
                });
            }
        });
    }
    function _setLoggedUserDataAndToken(user, res){
        console.log(user, user.userId, 'vlezna FB');
        var expires = moment().add(7, 'days').valueOf();
        var secret = app.get('jwtTokenSecret');
        var token = jwt.encode(
            {
                iss: user.userId,
                exp: expires
            },
            app.get('jwtTokenSecret')
        );

        res.json({
            success: true,
            user: user,
            token: token,
            expires: expires
        });
    }
    function registerVet(req, res, next){

        UserRepository.checkIfUserExists(req.body.email).then(function(isExisting){
            if (isExisting === true) {

                res.json({
                    success: false,
                    msg: "User already exists"
                });
            }
            else{

                UserRepository.registerVet({
                    email: req.body.email,
                    password: req.body.password,
                    name: req.body.name,
                    surname: req.body.surname,
                    place: req.body.placeId
                }).then(function(user){
                    UserRepository.linkVetWithPlace(user.userId, req.body.placeId).then(function(data){
                        res.json({
                            success: true,
                            user: user
                        });
                    }).catch(function(err){
                        console.log(err, 'linkVetPlace');
                    });
                }).catch(function(err){
                    console.log(err);
                });
            }
        });
    }
    function getLoggedUserData(req, res){
        console.log('>>>>>>>>>', req.user);
        async.parallel({
            user: Utils.asyncFromPromise(UserRepository.getUserDataById(req.user.id)),
            photos: Utils.asyncFromPromise(UserRepository.getUserPhotos(req.user.id))
        }, function(err, results) {
            if(err){
                console.log(err);
            } else {
               res.json(results);
            }

        });
    }
    function getUserData(req, res){
        async.parallel({
            user: Utils.asyncFromPromise(UserRepository.getUserDataById(req.params.id)),
            photos: Utils.asyncFromPromise(UserRepository.getUserPhotos(req.params.id))
        }, function(err, results) {
            if(err){
                console.log(err);
            } else {
               res.json(results);
            }
        });
    }
    function changePassword(req, res){
    	var userData = {
    		userId: req.user.id,
    		oldPass: req.body.oldPass,
    		newPass: req.body.newPass
    	};
        UserRepository.checkUserPassword(userData).then(function(isOldPassCorrect){
            if (isOldPassCorrect === true) {
                UserRepository.changePassword(userData).then(function(rr){
                    res.json({
                        success: true
                    });
                }).catch(function(err){
                    console.log('changePassword', err);
                });
            }
            else{
                res.json({
                    success: false,
                    msg: 'Wrong current password'
                });
            }
        }).catch(function(err){
            console.log('checkUserPassword', err);
        });
    }

    function updateUserData(req, res){
        var userData = {
            userId: req.user.id,
            phone: req.body.phone,
            name: req.body.name
        };
        UserRepository.updateUserData(userData).then(function() {
            async.parallel({
                user: Utils.asyncFromPromise(UserRepository.getUserDataById(req.user.id)),
                photos: Utils.asyncFromPromise(UserRepository.getUserPhotos(req.user.id))
            }, function(err, results) {
                if(err){
                    console.log(err);
                } else {
                   res.json(results);
                }

            });
        }).catch(function(err){
            console.log('changeUserData', err);
        });
    }

    function getUserNotifications(req, res, next) {
        UserRepository.getUserNotifications(req.user.id).then(function(notifications){
            res.json({notifications: notifications});
        }).catch(function(err){
            console.log('getUserNtf', err);
        })
    }

    function updateVetData(req, res){
        var userData = {
            userId: req.body.userId,
            email: req.body.email,
            phone: req.body.phone,
            name: req.body.name,
            surname: req.body.surname
        };
        UserRepository.updateUserData(userData).then(function(){
            UserRepository.updateVetPlace(userData.userId, req.body.placeId).then(function(data){
                res.json({
                    success: true,
                    data: data
                });
            }).catch(function(err){
                console.log('updateVetData', err);
            });
        }).catch(function(err){
            console.log('updateVetData', err);
        });
    }
    function changeEmail(req, res){
        var userData = {
            userId: req.user.id,
            newEmail: req.body.newEmail
        };
        UserRepository.changeEmail(userData).then(function(data){
            res.json({
                success: true,
                email: data.newEmail
            });
        }).catch(function(err){
            console.log('changeEmail', err);
        });
    }
    function changePhone(req, res){
        var userData = {
            userId: req.user.id,
            newPhone: req.body.newPhone
        };

        UserRepository.changePhone(userData).then(function(data){
            res.json({
                success: true,
                phone: data.newPhone
            });
        }).catch(function(err){
            console.log('changePhone', err);
        });
    }
    function changeNames(req, res){
        var userData = {
            userId: req.user.id,
            name: req.body.name,
            surname: req.body.surname
        };
        UserRepository.changeNames(userData).then(function(data){
            res.json({
                success: true,
                name: data.name,
                surname: data.surname
            });
        }).catch(function(err){
            console.log('changeNames', err);
        });
    }
    function forgottenPassword(req, res, next){
        UserRepository.checkIfUserExists(req.body.email, true).then(function(user){
            if (user.isExisting === true) {

                let newPass = (Math.floor(Math.random() * 9999) + 1000).toString();
                UserRepository.changePassword({
                    userId: user.userId,
                    newPass: newPass
                }).then(function(result){
                    sendResetPassEmail(req.body.email, newPass);

                    res.json({
                       success: true
                    });

                }).catch(function(err){
                    console.log('changePassword', err);
                });
            }
            else{
                res.json({
                    success: false,
                    msg: 'Не е намерен потребител с този e-mail.'
                });
            }
        });
    }



    function sendResetPassEmail(email, newPass){
            let transporter = nodemailer.createTransport({
                        service: config.email.service,
                        auth: {
                            user: config.email.user,
                            pass: config.email.pass
                        }
                    });
                    let mailOptions = {
                        from: 'chilldogapp@gmail.com', // sender address
                        to: email, // list of receivers
                        subject: 'Your new password for Chill ✔', // Subject line
                        text: 'Your new password is ' + newPass
                    };
                    transporter.sendMail(mailOptions, function(error, info){
                        if(error){
                            return console.log(error);
                        }
                        console.log('Message sent: ' + info.response);
                    });
    }

	function isLoggedIn (req, res, next) {
        /*var user = {
        	userId: 1,
        };
		req.logIn(user, function (error) {
			// console.log('args', arguments);

		});*/
		if (req.isAuthenticated()) {
			return next();
		}
		else {
			return res.status(403).end('Not Authenticated!');
		}
	}
    function getAllUsers (req, res, next) {
        UserRepository.getAllUsers().then(function(data){
            console.log(data);
            res.json({
                success: true,
                users: data
            });
        }).catch(function(err){
            console.log('getAllUsers', err);
        });
    }

    function getUsersByTerm(req, res, next) {
        UserRepository.getUsersByTerm(req.body.term).then(function(data){
            res.json({
                success: true,
                users: data
            });
        }).catch(function(err){
            console.log('getUsersByTerm', err);
        });
    }

    function getAllVets (req, res, next) {
        UserRepository.getAllVets().then(function(data){
            console.log(data);
            res.json({
                success: true,
                vets: data
            });
        }).catch(function(err){
            console.log('getAllUsers', err);
        });
    }
    function deleteVet(req, res, next){
        UserRepository.deleteVet(req.params.id).then(function(isDeleted){
            res.json({
                success: isDeleted
            });
        }).catch(function(err){
            console.error('deleteVet', err);
        });
    }
    function uploadFile(req, res, next){
        var file = req.files.file;
        var hashedString = crypto.createHash('md5').update(new Date().toString()).digest('hex');
        var extensionArray = file.type.split('/');
        var extension = extensionArray[1];
        var stream = fs.createReadStream(file.path);
        var fileName = hashedString + '.' + extension;
        return s3fsImpl.writeFile(fileName, stream, {"ContentType":"image/jpg"}).then(function(){
            fs.unlink(file.path, function(err){
                if(err){
                    console.error(err);
                    res.json({
                        success: false
                    });
                }
                UserRepository.uploadAvatarPhoto(fileName, req.user.id).then(function(data){
                    res.json({
                        success: true,
                        filename: data.imageUrl,
                        filepath: 'https://s3.amazonaws.com/vetsapp4/' + fileName
                    });
                });
                // console.log(file.path);
                // res.json({
                //     success: true
                // });
            });
        });
    }

    function uploadPhoto(req, res, next){
        var file = req.files.file;
        var hashedString = crypto.createHash('md5').update(new Date().toString()).digest('hex');
        var extensionArray = file.type.split('/');
        var extension = extensionArray[1];
        var stream = fs.createReadStream(file.path);
        var fileName = hashedString + '.' + extension;
        return s3fsImpl.writeFile(fileName, stream, {"ContentType":"image/jpg"}).then(function(){
            fs.unlink(file.path, function(err){
                if(err){
                    console.error(err);
                    res.json({
                        success: false
                    });
                }
                UserRepository.uploadPhoto(fileName, req.user.id, req.body).then(function(data){
                    res.json({
                        success: true,
                        filename: data.imageUrl,
                        filepath: 'https://s3.amazonaws.com/vetsapp4/' + fileName
                    });
                });
            });
        });
    }

    function togglePhotoPublic(req, res, next) {
        UserRepository.togglePhotoPublic(req.params.id).then(function(isUpdated){
            res.json({
                success: isUpdated
            });
        }).catch(function(err){
            console.error('togglePhotoPublic', err);
        });
    }

    function deletePhoto(req, res, next){
        UserRepository.deletePhoto(req.params.id).then(function(isDeleted){
            res.json({
                success: isDeleted
            });
        }).catch(function(err){
            console.error('deletePhoto', err);
        });
    }

    let subscribe = (req, res) => {
        if (req.body.email) {
            UserRepository.isEmailSubscribed(req.body.email).then(isSubscribed => {
                console.log(isSubscribed);
                if (isSubscribed === false) {
                    UserRepository.subscribeEmail(req.body.email).then(() => {
                        res.json({
                            success: true,
                            msg: 'You are subscribed!'
                        });
                    });
                }
                else {
                    res.json({
                        success: true,
                        msg: 'You are subscribed!'
                    });
                }
            });
        } else {

            res.json({
                success: false,
                msg: 'No email provided!'
            });
        }
    }

    function sendEmailWithData(email, templateData, subject) {

        ejs.renderFile(template, templateData, (err, html) => {
          if (err) console.log(err);

          let transporter = nodemailer.createTransport({
                        service: config.email.service,
                        auth: {
                            user: config.email.user,
                            pass: config.email.pass
                        }
                    });
                    let mailOptions = {
                        from: 'chilldogapp@gmail.com', // sender address
                        to: email, // list of receivers
                        subject: subject, // Subject line
                        html: html
                    };
                    transporter.sendMail(mailOptions, function(error, info){
                        if(error){
                            return console.log(error);
                        }
                        console.log('Message sent: ' + info.response);
                    });
        })
    }
    return {
        login: login,
        register: register,
        registerVet: registerVet,
        getLoggedUserData: getLoggedUserData,
        getUserData: getUserData,
        changeEmail: changeEmail,
        changeNames: changeNames,
        changePhone: changePhone,
        changePassword: changePassword,
        forgottenPassword: forgottenPassword,
        isLoggedIn: isLoggedIn,
        getAllUsers: getAllUsers,
        getAllVets: getAllVets,
        uploadFile: uploadFile,
        updateUserData: updateUserData,
        updateVetData: updateVetData,
        deleteVet: deleteVet,
        getUsersByTerm: getUsersByTerm,
        uploadPhoto: uploadPhoto,
        socialNetworkSignUp: socialNetworkSignUp,
        togglePhotoPublic: togglePhotoPublic,
        deletePhoto: deletePhoto,
        subscribe: subscribe,
        getUserNotifications: getUserNotifications
    };
};