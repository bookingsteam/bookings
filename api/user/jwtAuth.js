var url = require('url')
var mysql = require('../../lib/mysqlConnection/MysqlConnection'),
	config = require('../../config'),
	UserRepository = new(require('./repositories/UserRepository'))(mysql, config);
var jwt = require('jwt-simple');
var app = require('../../app');

module.exports = function(req, res, next){
	
	// Parse the URL, we might need this
	var parsed_url = url.parse(req.url, true)

	/**
	 * Take the token from:
	 * 
	 *  - the POST value access_token
	 *  - the GET parameter access_token
	 *  - the x-access-token header
	 *    ...in that order.

	 */
	var token = (req.body && req.body.access_token) || parsed_url.query.access_token || req.headers["x-access-token"];
	
	if (token) {


		try {
			var decoded = jwt.decode(token, app.get('jwtTokenSecret'))

			// if (decoded.exp <= Date.now()) {
			// 	res.end('Access token has expired', 400)				
			// }
			
			UserRepository.findUser(decoded.iss).then(function(users){
							
					req.user = users[0];	
					console.log(users, "users");						
					return next()
				
			})

		} catch (err) {	
			console.log(err);		
			res.json({
                success: false,
                msg: err
            }).catch(function(err){
                console.log('token', err);
            })
		}

	} else {


		res.json({
                success: false,
                msg: 'No token provided'
            }).catch(function(err){
                console.log('no-token', err);
            })

	}
}