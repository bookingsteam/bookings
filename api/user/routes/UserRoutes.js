var express = require('express');
var router = express.Router();
// var passport = require('passport');
var jwtauth = require('../jwtAuth.js');
var multiparty = require('connect-multiparty'),
    multipartyMiddleware = multiparty();

module.exports = function(UserController){

    router.get('/me', jwtauth, UserController.getLoggedUserData);
    router.get('/:id', jwtauth, UserController.getUserData);
    router.post('/me/changePassword', jwtauth, UserController.changePassword);
    router.post('/me/updateUserData', jwtauth, UserController.updateUserData);
    router.post('/login', UserController.login);
    router.post('/register', UserController.register);
    router.post('/register-social-network', UserController.socialNetworkSignUp);
    router.post('/forgottenPassword', UserController.forgottenPassword);
    router.get('/fblogin', function(req,res) {
        res.render('fblogin.ejs')
    });
    return router;
};
