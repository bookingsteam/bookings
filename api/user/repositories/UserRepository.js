var Q = require('q');
var bcrypt = require('bcryptjs');
var moment = require('moment');
module.exports = function UserRepository(mysql, config) {


    function loginUser(email, pass){
        var query = [
            'SELECT id, email, password, first_name, last_name, phone_number, photo, from_fb, register_date FROM users',
            'WHERE email=:email'
        ].join(" ");

        return mysql.makeQuery(query, {
            email: email,
            pass: pass,
        }, function(users){
            console.log(users);
            if (users.length === 0) {
                return false;
            }

            var loggedUser = null;
            var hash = '';
            users.forEach(function(user){
                if (bcrypt.compareSync(pass, user.password)) {
                    loggedUser = {
                        userId: user.id,
                        email: user.email,
                        first_name: user.first_name,
                        last_name: user.last_name,
                        phone: user.phone_number,
                        photo: user.photo,
                        register_date: user.register_date,
                        from_fb: user.from_fb,
                        loggedAt: new Date()
                    };
                    return false;
                }
            });
            return loggedUser;
            
        });
    }

    function setDeviceToken(token, userId){
        var query = [
            'UPDATE users',
            'SET device_id = :deviceToken',
            'WHERE id = :userId'
        ].join(" ");


        return mysql.makeQuery(query, {
            userId: userId,
            deviceToken: token
        }, function (result) {
            return result;
        });
    }

    function getUserDataById(userId){
        var query = [
            'SELECT id, email, first_name, last_name, phone_number, photo, register_date, from_fb FROM users',
            'WHERE id=:userId'
        ].join(" ");
        return mysql.makeQuery(query, {
            userId: userId,
        }, function (user) {
            return user;
        }, true);
    }

    function getUserPhotos(userId){
        var query = [
            'SELECT * FROM photos',
            'WHERE users_id = :userId AND is_deleted = 0;'
        ].join(" ");
        return mysql.makeQuery(query, {
            userId: userId
        }, function (result) {
            return result;
        });

    }

    function getUserNotifications(userId){
        var query = [
            'SELECT * FROM notifications',
            'WHERE target_user = :userId OR target_user = 0;'
        ].join(" ");
        return mysql.makeQuery(query, {
            userId: userId
        }, function (result) {
            return result;
        });
    }

    function getAllUsers(){
        var query = [
            'SELECT id, email, first_name, last_name, phone_number  FROM users;'
        ].join(" ");
        return mysql.makeQuery(query, null, function (result) {
            return result;
        });
    }

    function getUsersByTerm(term){
        var query = [
            'SELECT id, email, first_name, last_name, phone_number',
            'FROM users',
            'WHERE (users.first_name LIKE :term) OR (users.email LIKE :term);'
        ].join(" ");

        return mysql.makeQuery(query, {
            term: "%" + term + "%"
        }, function (result) {
            return result;
        });
    }

    function getAllVets(){
        var query = [
        'SELECT users.id, users.email, users.first_name, users.last_name, users.phone_number, places.name AS place_name, places.id AS place_id',
        'FROM users',
        'INNER JOIN places_has_users',
        'ON users.id = places_has_users.users_id',
        'INNER JOIN places',
        'ON places_has_users.places_id = places.id'
        ].join(" ");
        return mysql.makeQuery(query, null, function (result) {
            return result;
        });
    }

    function registerUser(userData){
        var query = [
            'INSERT INTO `vets`.`users` (`email`, `first_name`, `last_name`, `password`)',
            'VALUES (:email, :name, :surname, :password);'
        ].join(" ");

        // var salt = bcrypt.genSaltSync(10);
        var hash = bcrypt.hashSync(userData.password);

        return mysql.makeQuery(query, {
            email: userData.email,
            name: userData.name,
            surname: userData.surname,
            password: hash
        }, function (result) {
            return {
                userId: result.insertId,
                email: userData.email,
            };
        });
    }

    function registerUserBySocialNetwork(userData){
        var query = [
            'INSERT INTO `vets`.`users` (`email`, `password`, `first_name`, `photo`, `from_fb`)',
            'VALUES (:email, :password, :first_name, :photo, :fromFb);'
        ].join(" ");

        return mysql.makeQuery(query, {
            email: userData.email,
            first_name: userData.name,
            photo: userData.photo,
            password: '',
            fromFb: 1
        }, function (result) {
            return  result.insertId;
        });
    }
    function forceLogin(userId){
        var query = [
            'SELECT id, email, password, first_name, last_name, phone_number, photo, from_fb, register_date FROM users',
            'WHERE id=:userId',
            'LIMIT 1'
        ].join(" ");

        return mysql.makeQuery(query, {
            userId: userId
        }, function(users){
            if (users.length === 0) {
                return false;
            }
            let user = users[0];
            return {
                userId: user.id,
                email: user.email,
                first_name: user.first_name,
                last_name: user.last_name,
                phone: user.phone_number,
                photo: user.photo,
                register_date: user.register_date,
                from_fb: user.from_fb,
                loggedAt: new Date()
            };
            
        });
    }
    function getUserIdByEmail(email){
        var query = [
            'SELECT id FROM users',
            'WHERE email=:email',
            'LIMIT 1'
        ].join(" ");

        return mysql.makeQuery(query, {
            email: email
        }, function(users){
            if (users.length === 0) {
                return false;
            }
            return users[0].id;
        });
    }
    function registerVet(userData){
        var query = [
            'INSERT INTO `vets`.`users` (`email`, `first_name`, `last_name`, `password`, `is_doctor`)',
            'VALUES (:email, :name, :surname, :password, :isDoctor);'
        ].join(" ");

        // var salt = bcrypt.genSaltSync(10);
        var hash = bcrypt.hashSync(userData.password)
            bDay = new Date(userData.birthday);

        return mysql.makeQuery(query, {
            email: userData.email,
            name: userData.name,
            surname: userData.surname,
            password: hash,
            isDoctor: true
        }, function (result) {
            return {
                userId: result.insertId,
                email: userData.email,
            };
        });
    }
    function linkVetWithPlace(userId, placeId){
        var query = [
            'INSERT INTO `places_has_users`',
                '(`users_id`, `places_id`)',
            'VALUES (:userId, :placeId);'
        ].join(" ");


        return mysql.makeQuery(query, {
            userId: userId,
            placeId: placeId,
        }, function (result) {
            return result;
        });
    }
    function updateVetPlace(userId, placeId){
        var query = [
            'UPDATE places_has_users',
            'SET places_id = :placeId',
            'WHERE users_id = :userId'
        ].join(" ");


        return mysql.makeQuery(query, {
            userId: userId,
            placeId: placeId,
        }, function (result) {
            return result;
        });
    }
    function checkIfUserExists(email, returnId){
        var query = [
            'SELECT * FROM users',
            'WHERE email=:email'
        ].join(" ");

        return mysql.makeQuery(query, {
            email: email,
        }, function (result) {

            if (returnId === true) {
                return {
                    isExisting: result && result.length > 0,
                    userId: result[0] ? result[0].id : false
                };
            }
            else{
                return result && result.length > 0;
            }
        });
    }

    function isEmailSubscribed(email){
        var query = [
            'SELECT * FROM subscribers',
            'WHERE email=:email'
        ].join(" ");

        return mysql.makeQuery(query, {
            email: email,
        }, result => {
            return result && result.length > 0;
        });
    }

    function subscribeEmail(email){
        var query = [
            'INSERT INTO `vets`.`subscribers` (`email`)',
            'VALUES (:email);'
        ].join(" ");

        return mysql.makeQuery(query, {
            email: email,
        }, result => {
            return result.insertId;
        });
    }

    function checkUserPassword(data){
        var query = [
            'SELECT password FROM users WHERE id = :userId'
        ].join(" ");

        return mysql.makeQuery(query, {
            userId: data.userId,
        }, function(user){
            if (user && user.length === 1) {
                return bcrypt.compareSync(data.oldPass, user[0].password);
            }
            else{
                return false;
            }
        });
    }
    function changePassword(data){
        var query = [
            'UPDATE users',
            'SET password = :newPass',
            'WHERE id = :userId'
        ].join(" ");

        return mysql.makeQuery(query, {
            userId: data.userId,
            newPass: bcrypt.hashSync(data.newPass)
        });
    }
    function changeNames(data){
        var query = [
            'UPDATE users',
            'SET first_name = :name, last_name = :surname',
            'WHERE id = :userId'
        ].join(" ");
        console.log(data);
        return mysql.makeQuery(query, {
            userId: data.userId,
            name: data.name,
            surname: data.surname
        }, function (result) {
            return {
                userId: result.insertId,
                name: data.name,
                surname: data.surname
            };
        });
    }
    function changeEmail(data){
        var query = [
            'UPDATE users',
            'SET email = :newEmail',
            'WHERE id = :userId'
        ].join(" ");
        console.log(data);
        return mysql.makeQuery(query, {
            userId: data.userId,
            newEmail: data.newEmail
        }, function (result) {
            return {
                userId: result.insertId,
                newEmail: data.newEmail
            };
        });
    }
    function changePhone(data){
        var query = [
            'UPDATE users',
            'SET phone_number = :newPhone',
            'WHERE id = :userId'
        ].join(" ");
        console.log(data);
        return mysql.makeQuery(query, {
            userId: data.userId,
            newPhone: data.newPhone
        }, function (result) {
            return {
                userId: result.insertId,
                newPhone: data.newPhone
            };
        });
    }
    function updateUserData(data){
        var query = [
            'UPDATE users',
            'SET phone_number = :phone, first_name = :name',
            'WHERE id = :userId'
        ].join(" ");
        console.log(data);
        return mysql.makeQuery(query, {
            userId: data.userId,
            phone: data.phone,
            name: data.name
        }, function (result) {
            return result;
        });
    }
    function uploadAvatarPhoto(filename, userId){
        var query = [
            'UPDATE users',
            'SET photo = :imageUrl',
            'WHERE id = :userId'
        ].join(" ");

        return mysql.makeQuery(query, {
            imageUrl: filename,
            userId: userId
        }, function (result) {
            return {
                photoId: result.insertId,
                imageUrl: filename
            };
        });
    }

    function uploadPhoto(filename, userId, photoData){

        var query = [
            'INSERT INTO `vets`.`photos` (`users_id`, `treatments_id`, `walks_id`, `url`, `is_public`)',
            'VALUES (:userId, :treatmentId, :walkId, :photoUrl, :isPublic);'
        ].join(" ");

        return mysql.makeQuery(query, {
            userId: userId,
            treatmentId: photoData.treatmentId,
            walkId: photoData.walkId,
            photoUrl: filename,
            isPublic: photoData.isPublic ? 1 : 0
        }, function (result) {
            return {
                photoId: result.insertId,
                imageUrl: filename
            };
        });
    }

    function togglePhotoPublic(photoId){
        var query = [
            'UPDATE photos',
            'SET is_public = IF(is_public=1, 0, 1)',
            'WHERE id = :photoId'
        ].join(" ");
    
        return mysql.makeQuery(query, {
            photoId: photoId
        }, function (result) {
            return result.affectedRows > 0;
        });
    }

    function deletePhoto(photoId){
        var query = [
            'UPDATE photos',
            'SET is_deleted = 1',
            'WHERE id = :photoId'
        ].join(" ");
  
        return mysql.makeQuery(query, {
            photoId: photoId
        }, function (result) {
            return result.affectedRows > 0;
        });
    }

    function findUser(userId){
        var query = [
            'SELECT * FROM users',
            'WHERE id=:userId'
        ].join(" ");

        return mysql.makeQuery(query, {
            userId: userId,
        }, function (user) {
            return user;
        });
    }
    function deleteVet(vetId){
        var query = [
            'DELETE FROM `users`',
            'WHERE id = :id'
        ].join(" ");

        return mysql.makeQuery(query, {
            id: vetId
        }, function(result){
            return result.affectedRows === 1;
        });
    }

    return {
        loginUser: loginUser,
        getUserDataById: getUserDataById,
        registerUser: registerUser,
        registerVet: registerVet,
        linkVetWithPlace: linkVetWithPlace,
        findUser: findUser,
        checkIfUserExists: checkIfUserExists,
        checkUserPassword: checkUserPassword,
        changePassword: changePassword,
        changeEmail: changeEmail,
        changeNames: changeNames,
        changePhone: changePhone,
        getAllUsers: getAllUsers,
        getAllVets: getAllVets,
        uploadAvatarPhoto: uploadAvatarPhoto,
        updateUserData: updateUserData,
        deleteVet: deleteVet,
        updateVetPlace: updateVetPlace,
        getUsersByTerm: getUsersByTerm,
        getUserPhotos: getUserPhotos,
        uploadPhoto: uploadPhoto,
        registerUserBySocialNetwork: registerUserBySocialNetwork,
        forceLogin: forceLogin,
        getUserIdByEmail: getUserIdByEmail,
        togglePhotoPublic: togglePhotoPublic,
        deletePhoto: deletePhoto,
        isEmailSubscribed: isEmailSubscribed,
        subscribeEmail: subscribeEmail,
        setDeviceToken: setDeviceToken,
        getUserNotifications: getUserNotifications
    };
}