
var mysql = require('../lib/mysqlConnection/MysqlConnection'),
	config = require('../config');
	NotificationsRepository = new(require('./notifications/repositories/NotificationsRepository'))(mysql, config),
	NotificationsController = new (require('./notifications/controllers/NotificationsController'))(NotificationsRepository);


module.exports = function NotificationsManager() {

	function testMeth() {
		
	}

	function createNotification(data, type){
		console.log("CREATE MNGR");
		NotificationsController.createNotificationFromData(data, type);
	}

	return {
		testMeth: testMeth,
		createNotification: createNotification
	}
}