
var apn = require('apn');
var options = {
  token: {
    key: "api/APNsAuthKey_4YA389Z3WH.p8",
    keyId: "4YA389Z3WH",
    teamId: "H6KAAZFWNY"
  },
  production: true
};

var apnProvider = new apn.Provider(options);

module.exports = function NotificationSender() {

	function sendUrlNotification(data) {
        var note = new apn.Notification();
		note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
        note.badge = 0;
        note.sound = "ping.aiff";
        note.alert =  data.content;
        note.payload = {'messageFrom': 'John Appleseed'};
        note.topic = "com.tdermendjiev.Chill";

        apnProvider.send(note, data.deviceId).then( (result) => {
          console.log(result);
        });
	}

	function sendShareNotification(ntfData) {
      
        var note = new apn.Notification();

        note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
        note.badge = 1;
        note.sound = "ping.aiff";
        note.alert = ntfData.alert
        note.payload = {
            'content': ntfData.alert,
            'type' : 2
        };
        note.topic = "com.tdermendjiev.Chill";

        apnProvider.send(note, ntfData.deviceId).then( (result) => {
          console.log(result);
        });
          
    }

	return {
		sendUrlNotification: sendUrlNotification,
		sendShareNotification: sendShareNotification
	}
}