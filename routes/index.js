var router = require('express').Router(),
    path = require('path'),
    pathServer = path.join(__dirname, '../'),
    pathLib = pathServer + 'lib/',
    config = require('../config'),
    apiDir = 'api/',
    mainController = require('../controllers/main')();
    googleAuth = require('../utils/googleAuthManager');

module.exports = function(app) {

    /**************REST API********************/

    app.use('/', router);

    pathServer += apiDir;

    var user = require(pathServer + 'user')(app);
    var calendar = require(pathServer + 'calendar')();
    var notifications = require(pathServer + 'notifications')(app);

    // var dewormings = require(pathServer + 'dewormings')(app);
    // var treatments = require(pathServer + 'treatments')(app);
    // var vaccines = require(pathServer + 'vaccines')(app);
    app.get('/', mainController.index);
    app.get('/schedule',  googleAuth.authorize, mainController.schedule);
    app.get('/test', mainController.forTests);
    app.get('/screen', mainController.screen);
    app.get('/token', googleAuth.getTokens);
    app.get('/checkVersion', checkVersion);

    app.get('/reset-password', mainController.displayResetPassword);
    app.get('/change-password', mainController.displayChangePassword);

    app.use(config.apiPrefix + '/user', user);
    app.use(config.apiPrefix + '/calendar', calendar);
    app.use(config.apiPrefix + '/notifications', notifications);

    //comment
    function checkVersion(req, res, next) {
    res.json({currentVersion: "0.9.4"});
    }

    return router;
};
